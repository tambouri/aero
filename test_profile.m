function [theory,relative_error] = test_profile(profile,measured_cl,sum_cp,pivot_point,AoA,X,beta)

%THIN FOIL 1
phi=acos(2*(1-pivot_point)-1);
Cl_fix = 2*pi*deg2rad(AoA);
A = extract_An([X, profile(:,1)],2);
A0 = A(1);
A1 = A(2);
Cl_flap = pi*(A1-2*A0);
results_1 = Cl_fix + Cl_flap;
% error with datas
theory(1,:) = [(results_1(7,1)-results_1(6,1))/AoA(7,1), Cl_flap];
relative_error(1,:) = (theory(1,:)-measured_cl)./measured_cl;

% THIN FOIL 2
phi=acos(2*(1-pivot_point)-1);

results_1(:,3)=2*pi*deg2rad(AoA)+2*(pi-phi+sin(phi))*deg2rad(beta);
theory(2,:)=[(results_1(7,3)-results_1(6,3))/AoA(7,1),results_1(6,3)];
relative_error(2,:) = (theory(2,:)-measured_cl)./measured_cl;


%JOUKOWSKI
angle_offset = atan(-profile(1,1));
c_ratio=max(profile(:,5));
t_ratio=max(profile(:,2)-profile(:,3));
results_1(:,4) = 2*pi*(deg2rad(AoA)+angle_offset+2*c_ratio)*(1-4/(3*sqrt(3))*t_ratio)*(1+0.5*(2*c_ratio/(1-4/(3*sqrt(3))*t_ratio))^2);
theory(3,:)=[(results_1(7,4)-results_1(6,4))/AoA(7,1),results_1(6,4)];
relative_error(3,:)= (theory(3,:)-measured_cl)./measured_cl;

% CP THIN
n=8; % 1 to 9
u_inf=20;
sz_theta=sum_cp(1);
theta = linspace(0,pi,sz_theta)';

% unique x axis for all plots
results_3(:,1) = (1-cos(theta))/2;

alpha = deg2rad(0)+atan(-profile(1,1));
results_3(:,2) = cp_thinfoil(profile(:,4:5), n, u_inf, theta, alpha);
relative_error(4,:) = ones(1,2)*(sum(results_3(2:end,2))-sum_cp(2,1))/sum_cp(2,1);

alpha = deg2rad(2)+atan(-profile(1,1));
results_3(:,3) = cp_thinfoil(profile(:,4:5), n, u_inf, theta, alpha);
relative_error(5,:) = ones(1,2)*(sum(results_3(2:end,3))-sum_cp(3,1))/sum_cp(3,1);

alpha = deg2rad(4)+atan(-profile(1,1));
results_3(:,4) = cp_thinfoil(profile(:,4:5), n, u_inf, theta, alpha);
relative_error(6,:) = ones(1,2)*(sum(results_3(2:end,4))-sum_cp(4,1))/sum_cp(4,1);

alpha = deg2rad(6)+atan(-profile(1,1));
results_3(:,5) = cp_thinfoil(profile(:,4:5), n, u_inf, theta, alpha);
relative_error(7,:) = ones(1,2)*(sum(results_3(2:end,5))-sum_cp(5,1))/sum_cp(5,1);
