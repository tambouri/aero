clc; 
close all;
clear all;

%% DATAS LECTURE
% inport profile
NACA0012 = readmatrix('NACA0012.csv');

% inport paper datas
% Cl vs AoA
datas_cl = readmatrix('datas_cl.csv');
AoA = datas_cl(:,1);
disp("Angle 0 is the element "+string(find(AoA==0))+" in the vector AoA")

% Cp vs x/c
datas_cp = readmatrix('datas_cp_alpha0.csv');
datas_cp(:,7:12) = readmatrix('datas_cp_alpha2.csv');
datas_cp(:,7:8) =   [];
datas_cp(:,11:16) = readmatrix('datas_cp_alpha4.csv');
datas_cp(:,11:12) = [];
datas_cp(:,15:20) = readmatrix('datas_cp_alpha6.csv');
datas_cp(:,15:16) = [];

% integral error on Cp (ignoring first term for comparison with simulation)
sum_cp_s=[height(datas_cp(:,1));sum(datas_cp(2:end,3)-datas_cp(2:end,5));sum(datas_cp(2:end,7)-datas_cp(2:end,9));sum(datas_cp(2:end,11)-datas_cp(2:end,13));sum(datas_cp(2:end,15)-datas_cp(2:end,17))];
sum_cp_c=[height(datas_cp(2:end,1));sum(datas_cp(2:end,4)-datas_cp(2:end,6));sum(datas_cp(2:end,8)-datas_cp(2:end,10));sum(datas_cp(2:end,12)-datas_cp(2:end,14));sum(datas_cp(2:end,16)-datas_cp(2:end,18))];

% fit the measurement
measured(1,:) = coeffvalues(fit(datas_cl(1:19,1),datas_cl(1:19,2), 'poly'+string(1)));
measured(2,:) = coeffvalues(fit(datas_cl(1:19,1),datas_cl(1:19,3), 'poly'+string(1)));
measured(3,:) = coeffvalues(fit(datas_cl(1:19,1),datas_cl(1:19,4), 'poly'+string(1)));
measured(4,:) = coeffvalues(fit(datas_cl(1:19,1),datas_cl(1:19,5), 'poly'+string(1)));


%% profile formation (type, beta, curve_y)
% original profile
X=NACA0012(:,1)./NACA0012(1,1);
profile_0=[zeros(height(X),1), NACA0012(:,2), -1.*NACA0012(:,2)]./NACA0012(1,1); %[camber, up, down]

% constant parameters in the paper
pivot_point=0.7;
n=2.5;

% create a deformed profile beta = 5° straight
profile_5s = deform(X,"straight", 5, profile_0, pivot_point,n);

% create a deformed profile beta = 5° curved
profile_5c = deform(X,"curved", 5, profile_0, pivot_point,n);

% create a deformed profile beta = 10° straight
profile_10s = deform(X,"straight", 10, profile_0, pivot_point,n);

% create a deformed profile beta = 10° curved
profile_10c = deform(X,"curved", 10, profile_0, pivot_point,n);

%% plot wings
%close all

% plot profiles beta = 5°
fig = figure;
set(fig, 'Name', 'profiles overview');
%----------------------------------WINGS-----------------------------------
subplot(2,1,1);
hold on;
plot([profile_5c(:,6), profile_5c(:,8)],[profile_5c(:,7), profile_5c(:,9)], "Color",[0 0 0])
plot([profile_5s(:,6), profile_5s(:,8)],[profile_5s(:,7), profile_5s(:,9)], "Color",[0 0 0], "LineStyle","--")%
plot([profile_5c(:,4)],[profile_5c(:,5)], "Color","r")%, "LineStyle","--"
plot([profile_5s(:,4), profile_5c(:,4)],[profile_5s(:,5), profile_5c(:,5)], "Color","r", "LineStyle","--")%
grid on;
axis equal;
title( '5 beta angle profiles');
xlabel('x/c ');
ylabel('y/c ');
%----------------------------------FLAPS-----------------------------------
subplot(2,1,2)
hold on
plot(X,profile_5s(:,2), "Color",[0 0 0], "LineStyle","--")
plot(X,profile_5c(:,2), "Color",[0 0 0])
plot(X,profile_5s(:,1), "Color","r", "LineStyle","--")
plot(X,profile_5c(:,1), "Color","r")
plot(X,profile_5c(:,3), "Color",[0 0 0])
plot(X,profile_5s(:,3), "Color",[0 0 0], "LineStyle","--")
grid on
xlim([pivot_point, 1]);
title( '5 beta angle flaps (not redressed)');
xlabel('x/c ');
ylabel('y/c ');
legend("straight profile", "curved profile", "camberline straight", "camberline curved")

% plot profiles beta = 10
fig = figure;
set(fig, 'Name', 'profiles overview');
%----------------------------------WINGS-----------------------------------
subplot(2,1,1);
plot([profile_10c(:,6), profile_10c(:,8)],[profile_10c(:,7), profile_10c(:,9)], "Color",[0 0 0])
hold on;
plot([profile_10s(:,6), profile_10s(:,8)],[profile_10s(:,7), profile_10s(:,9)], "Color",[0 0 0], "LineStyle","--")%
plot([profile_10c(:,4)],[profile_10c(:,5)], "Color","r")%, "LineStyle","--"
plot([profile_10s(:,4), profile_10c(:,4)],[profile_10s(:,5), profile_10c(:,5)], "Color","r", "LineStyle","--")%
grid on;
axis equal;
title( '10 beta angle profiles');
xlabel('x/c ');
ylabel('y/c ');
%----------------------------------FLAPS-----------------------------------
subplot(2,1,2)
hold on
plot(X,profile_10s(:,2), "Color",[0 0 0], "LineStyle","--")
plot(X,profile_10c(:,2), "Color",[0 0 0])
plot(X,profile_10s(:,1), "Color","r", "LineStyle","--")
plot(X,profile_10c(:,1), "Color","r")
plot(X,profile_10c(:,3), "Color",[0 0 0])
plot(X,profile_10s(:,3), "Color",[0 0 0], "LineStyle","--")
grid on
xlim([pivot_point, 1]);
title( '10 beta angle flaps (not redressed)');
xlabel('x/c ');
ylabel('y/c ');
legend("straight profile", "curved profile", "camberline straight", "camberline curved")

%% THIN AIRFOIL THEORY 1 Cl
% fix wing lift common to all profiles
Cl_fix = 2*pi*deg2rad(AoA);

% plot polyfitted profiles
theta = linspace(0,pi,100)';
n=2;
%PLOT GROUPED 
fig = figure('WindowState', 'maximized');
sgtitle('fitted profiles by polynom deg '+string(n))
%--------------------------------------------------------------------------
subplot(2,1,1)
title("straight and curved beta 5")
hold on
plot(X,profile_10s(:,1))
plot(X,profile_10c(:,1))
c = coeffvalues(fit(X,profile_10s(:,1), 'poly'+string(n)));
fun0 =  @(theta) ((1-cos(theta))/2).^[n:-1:0]*c';
plot((1-cos(theta))/2,fun0(theta))
c = coeffvalues(fit(X,profile_10c(:,1), 'poly'+string(n)));
fun0 =  @(theta) ((1-cos(theta))/2).^[n:-1:0]*c';
plot((1-cos(theta))/2,fun0(theta))
%axis equal
xlabel('X/c ');
ylabel('Y/c ');
grid on
legend("camberline straight","camberline curved","fit straight","fit curved")
%--------------------------------------------------------------------------
subplot(2,1,2)
title("straight and curved beta 10")
hold on
plot(X,profile_5s(:,1))
plot(X,profile_5c(:,1))
c = coeffvalues(fit(X,profile_5s(:,1), 'poly'+string(n)));
fun0 =  @(theta) ((1-cos(theta))/2).^[n:-1:0]*c';
plot((1-cos(theta))/2,fun0(theta))
c = coeffvalues(fit(X,profile_5c(:,1), 'poly'+string(n)));
fun0 =  @(theta) ((1-cos(theta))/2).^[n:-1:0]*c';
plot((1-cos(theta))/2,fun0(theta))
xlabel('X/c ');
ylabel('Y/c ');
%axis equal
grid on
legend("camberline straight","camberline curved","fit straight","fit curved")


% beta = 5° straight
A = extract_An([X, profile_5s(:,1)],2);
A0 = A(1);
A1 = A(2);
Cl_flap = pi*(A1-2*A0);
results_1 = Cl_fix + Cl_flap;
% error with datas
theory(1,1,:) = [2*pi*deg2rad(1), Cl_flap];
relative_error(1,1,:) = (squeeze(theory(1,1,:))-measured(1,:)')./measured(1,:)';

% beta = 5° curved
A = extract_An([X, profile_5c(:,1)],2);
A0 = A(1);
A1 = A(2);
Cl_flap = pi*(A1-2*A0);
results_1(:,2) = Cl_fix + Cl_flap;
% error with datas
theory(1,2,:) = [2*pi*deg2rad(1), Cl_flap];
relative_error(1,2,:) = (squeeze(theory(1,2,:))-measured(2,:)')./measured(2,:)';

% beta = 10° straight
A = extract_An([X, profile_10s(:,1)],2);
A0 = A(1);
A1 = A(2);
Cl_flap = pi*(A1-2*A0);
results_2 = Cl_fix + Cl_flap;
% error with datas
theory(1,3,:) = [2*pi*deg2rad(1), Cl_flap];
relative_error(1,3,:) = (squeeze(theory(1,3,:))-measured(3,:)')./measured(3,:)';

% beta = 10° curved
A = extract_An([X, profile_10c(:,1)],2);
A0 = A(1);
A1 = A(2);
Cl_flap = pi*(A1-2*A0);
results_2(:,2) = Cl_fix + Cl_flap;
% error with datas
theory(1,4,:) = [2*pi*deg2rad(1), Cl_flap];
relative_error(1,4,:) = (squeeze(theory(1,4,:))-measured(4,:)')./measured(4,:)';

%% THIN AIRFOIL THEORY 2  Cl
phi=acos(2*(1-pivot_point)-1);

% beta = 5° straight
results_1(:,3)=2*pi*deg2rad(AoA)+2*(pi-phi+sin(phi))*deg2rad(5);
theory(2,1,:)=[(results_1(7,3)-results_1(6,3))/AoA(7,1),results_1(6,3)];
relative_error(2,1,:) = (squeeze(theory(2,1,:))-measured(1,:)')./measured(1,:)';
relative_error(2,2,:)= (squeeze(theory(2,1,:))-measured(2,:)')./measured(2,:)';

% beta = 10° straight
results_2(:,3)=2*pi*deg2rad(AoA)+2*(pi-phi+sin(phi))*deg2rad(10);
theory(2,3,:)=[(results_2(7,3)-results_2(6,3))/AoA(7,1),results_2(6,3)];
relative_error(2,3,:)= (squeeze(theory(2,3,:))-measured(3,:)')./measured(3,:)';
relative_error(2,4,:)= (squeeze(theory(2,3,:))-measured(4,:)')./measured(4,:)';

%% JOUKOWSKI THEORY
% beta = 5° straight
angle_offset5 = atan(-profile_5s(1,1));
c_ratio=max(profile_5s(:,5));
t_ratio=max(profile_5s(:,2)-profile_5s(:,3));
results_1(:,4) = 2*pi*(deg2rad(AoA)+angle_offset5+2*c_ratio)*(1-4/(3*sqrt(3))*t_ratio)*(1+0.5*(2*c_ratio/(1-4/(3*sqrt(3))*t_ratio))^2);
theory(3,1,:)=[(results_1(7,4)-results_1(6,4))/AoA(7,1),results_1(6,4)];
relative_error(3,1,:)= (squeeze(theory(3,1,:))-measured(1,:)')./measured(1,:)';

% beta = 5° curved
angle_offset5 = atan(-profile_5c(1,1));
c_ratio=max(profile_5c(:,5));
t_ratio=max(profile_5c(:,2)-profile_5c(:,3));
results_1(:,5) = 2*pi*(deg2rad(AoA)+angle_offset5+2*c_ratio)*(1-4/(3*sqrt(3))*t_ratio)*(1+0.5*(2*c_ratio/(1-4/(3*sqrt(3))*t_ratio))^2);
theory(3,2,:)=[(results_1(7,5)-results_1(6,5))/AoA(7,1),results_1(6,5)];
relative_error(3,2,:)= (squeeze(theory(3,2,:))-measured(2,:)')./measured(2,:)';

% beta = 10° straight
angle_offset10 = atan(-profile_10s(1,1));
c_ratio = max(profile_10s(:,5));
t_ratio=max(profile_10s(:,2)-profile_10s(:,3));
results_2(:,4) = 2*pi*(deg2rad(AoA)+angle_offset10+2*c_ratio)*(1-4/(3*sqrt(3))*t_ratio)*(1+0.5*(2*c_ratio/(1-4/(3*sqrt(3))*t_ratio))^2);
theory(3,3,:)=[(results_2(7,4)-results_2(6,4))/AoA(7,1),results_2(6,4)];
relative_error(3,3,:)= (squeeze(theory(3,3,:))-measured(3,:)')./measured(3,:)';

% beta = 10° curved
angle_offset10 = atan(-profile_10c(1,1));
c_ratio = max(profile_10c(:,5));
t_ratio=max(profile_10c(:,2)-profile_10c(:,3));
results_2(:,5) = 2*pi*(deg2rad(AoA)+angle_offset10+2*c_ratio)*(1-4/(3*sqrt(3))*t_ratio)*(1+0.5*(2*c_ratio/(1-4/(3*sqrt(3))*t_ratio))^2);
theory(3,4,:)=[(results_2(7,5)-results_2(6,5))/AoA(7,1),results_2(6,5)];
relative_error(3,4,:)= (squeeze(theory(3,4,:))-measured(4,:)')./measured(4,:)';


%% cp with thin airfoil theory
n=8; % 1 to 9
u_inf=20;
sz_theta=height(datas_cp);
theta = linspace(0,pi,sz_theta)';

% unique x axis for all plots
results_3(:,1) = (1-cos(theta))/2;

% plot fitted profiles
figure
hold on
plot(X,profile_10s(:,1))
plot(X,profile_10c(:,1))
c = coeffvalues(fit(X,profile_10s(:,1), 'poly'+string(n)));
fun0 =  @(theta) ((1-cos(theta))/2).^[n:-1:0]*c';
plot((1-cos(theta))/2,fun0(theta))
c = coeffvalues(fit(X,profile_10c(:,1), 'poly'+string(n)));
fun0 =  @(theta) ((1-cos(theta))/2).^[n:-1:0]*c';
plot((1-cos(theta))/2,fun0(theta))
xlabel('x/c');
ylabel('y/c ');
grid on
legend("camberline straight","camberline curved","fit straight","fit curved")


% for each alpha curved and straight solution
alpha = deg2rad(0)+atan(-profile_10s(1,1));
results_3(:,2) = cp_thinfoil(profile_10s(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,1)=(sum(results_3(2:end,2))-sum(datas_cp(2:end,3)-datas_cp(2:end,5)))/sum(datas_cp(2:end,3)-datas_cp(2:end,5));

alpha = deg2rad(0)+atan(-profile_10c(1,1));
results_3(:,3) = cp_thinfoil(profile_10c(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,2)=(sum(results_3(2:end,3))-sum(datas_cp(2:end,4)-datas_cp(2:end,6)))/sum(datas_cp(2:end,4)-datas_cp(2:end,6));

alpha = deg2rad(2)+atan(-profile_10s(1,1));
results_3(:,4) = cp_thinfoil(profile_10s(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,3)=(sum(results_3(2:end,4))-sum(datas_cp(2:end,3)-datas_cp(2:end,5)))/sum(datas_cp(2:end,3)-datas_cp(2:end,5));

alpha = deg2rad(2)+atan(-profile_10c(1,1));
results_3(:,5) = cp_thinfoil(profile_10c(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,4)=(sum(results_3(2:end,5))-sum(datas_cp(2:end,4)-datas_cp(2:end,6)))/sum(datas_cp(2:end,4)-datas_cp(2:end,6));

alpha = deg2rad(4)+atan(-profile_10s(1,1));
results_3(:,6) = cp_thinfoil(profile_10s(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,5)=(sum(results_3(2:end,6))-sum(datas_cp(2:end,3)-datas_cp(2:end,5)))/sum(datas_cp(2:end,3)-datas_cp(2:end,5));

alpha = deg2rad(4)+atan(-profile_10c(1,1));
results_3(:,7) = cp_thinfoil(profile_10c(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,6)=(sum(results_3(2:end,7))-sum(datas_cp(2:end,4)-datas_cp(2:end,6)))/sum(datas_cp(2:end,4)-datas_cp(2:end,6));

alpha = deg2rad(6)+atan(-profile_10s(1,1));
results_3(:,8) = cp_thinfoil(profile_10s(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,7)=(sum(results_3(2:end,8))-sum(datas_cp(2:end,3)-datas_cp(2:end,5)))/sum(datas_cp(2:end,3)-datas_cp(2:end,5));

alpha = deg2rad(6)+atan(-profile_10c(1,1));
results_3(:,9) = cp_thinfoil(profile_10c(:,4:5), n, u_inf, theta, alpha);
relative_errorcp(:,8)=(sum(results_3(2:end,9))-sum(datas_cp(2:end,4)-datas_cp(2:end,6)))/sum(datas_cp(2:end,4)-datas_cp(2:end,6));


%% RESULTS
%close all

%plot datas & results
fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'reuslts Clvs AoA with stall');
sgtitle('Cl vs Angle of attack')
%-----------------------beta 5
subplot(1,2,1);
plot(datas_cl(:,1), [datas_cl(:,2:3), results_1])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Resulting Cl vs Angle of Attack for beta = 5');
xlabel('AoA [deg]');
ylabel('Cl ');
legend('data straight', 'data curved', "thin airfoil method 1 straight", "thin airfoil method 1 curved", "thin airfoil method 2", "joukowski method straight", "joukowski method curved");
%-----------------------beta 10
subplot(1,2,2);
plot(datas_cl(:,1), [datas_cl(:,4:5), results_2])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Resulting Cl vs Angle of Attack for beta =  10');
xlabel('AoA [deg]');
ylabel('Cl ');
legend('data straight', 'data curved', "thin airfoil method 1 straight", "thin airfoil method 1 curved", "thin airfoil method 2", "joukowski method straight", "joukowski method curved");

% plot erreur cp vs x/c
fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'errCp vs x/c');
sgtitle('delta Cp vs x/c')
%-------------------- alpha 0
subplot(2,2,1);
hold on
plot(results_3(:,1),results_3(:,2),"Color", 'c')
plot(results_3(:,1),results_3(:,3),"Color", 'm')
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,3)-datas_cp(:,5),"Color","b")
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,4)-datas_cp(:,6),"Color","r")
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
ylim([0, 3]);
title('Resulting deltaCp vs cord for alpha = 0');
xlabel('x/c ');
ylabel('deltaCp ');
%-------------------- alpha 2
subplot(2,2,2)
hold on
plot(results_3(:,1),results_3(:,4),"Color", 'c')
plot(results_3(:,1),results_3(:,5),"Color", 'm')
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,7)-datas_cp(:,9),"Color","b")
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,8)-datas_cp(:,10),"Color","r")
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
ylim([0, 3]);
title('Resulting deltaCp vs cord for alpha = 2');
xlabel('x/c ');
ylabel('deltaCp ');
%-------------------- alpha 4
subplot(2,2,3);
hold on
plot(results_3(:,1),results_3(:,6),"Color", 'c')
plot(results_3(:,1),results_3(:,7),"Color", 'm')
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,11)-datas_cp(:,13),"Color","b")
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,12)-datas_cp(:,14),"Color","r")
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
ylim([0, 3]);
title('Resulting deltaCp vs cord for alpha = 4');
xlabel('x/c ');
ylabel('deltaCp ');
%-------------------- alpha 6
subplot(2,2,4)
hold on
plot(results_3(:,1),results_3(:,8),"Color", 'c')
plot(results_3(:,1),results_3(:,9),"Color", 'm')
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,15)-datas_cp(:,17),"Color","b")
plot((datas_cp(:,1)+datas_cp(:,2))/2, datas_cp(:,16)-datas_cp(:,18),"Color","r")
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
ylim([0, 3]);
title('Resulting delta Cp vs cord for alpha = 6');
xlabel('x/c ');
ylabel('deltaCp ');
legend('thin foil theory straight', 'thin foil theory curved', 'measured straight',  'measured curved');


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'results Cl witout stall');
sgtitle('Cl vs Angle of attack (without stall)')
%-----------------------beta 5 straight
subplot(2,2,1);
plot(datas_cl(1:19,1), [datas_cl(1:19,2),results_1(1:19,[1,3,4])])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Resulting Cl vs Angle of Attack for beta = 5 straight');
xlabel('AoA [deg]');
ylabel('Cl ');
legend('data', "thin airfoil method 1","thin airfoil method 2", "joukowski method ");
%-----------------------beta 5 curved
subplot(2,2,2);
plot(datas_cl(1:19,1), [datas_cl(1:19,3),results_1(1:19,[2,5])])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Resulting Cl vs Angle of Attack for beta = 5 curved');
xlabel('AoA [deg]');
ylabel('Cl ');
legend('data', "thin airfoil method 1", "joukowski method ");
%-----------------------beta 10 straight
subplot(2,2,3);
plot(datas_cl(1:19,1), [datas_cl(1:19,4),results_2(1:19,[1,3,4])])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Resulting Cl vs Angle of Attack for beta =  10 straight');
xlabel('AoA [deg]');
ylabel('Cl ');
legend('data', "thin airfoil method 1","thin airfoil method 2", "joukowski method ");
%-----------------------beta 10 curved
subplot(2,2,4);
plot(datas_cl(1:19,1), [datas_cl(1:19,5),results_2(1:19,[2,5])])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Resulting Cl vs Angle of Attack for beta =  10 curved');
xlabel('AoA [deg]');
ylabel('Cl ');
legend('data', "thin airfoil method 1","joukowski method ");


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'Brute error Cl');
sgtitle('errors on Cl vs Angle of attack')
%-----------------------beta 5 straight
subplot(2,2,1);
plot(datas_cl(1:19,1), [results_1(1:19,[1,3,4])-datas_cl(1:19,2)])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Error Cl vs Angle of Attack for beta = 5 straight');
xlabel('AoA [deg]');
ylabel('Cl ');
legend("thin airfoil method 1","thin airfoil method 2", "joukowski method");
%-----------------------beta 5 curved
subplot(2,2,2);
plot(datas_cl(1:19,1), [results_1(1:19,[2,5])-datas_cl(1:19,3)])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Error Cl vs Angle of Attack for beta = 5 curved');
xlabel('AoA [deg]');
ylabel('Cl ');
legend("thin airfoil method 1","joukowski method");
%-----------------------beta 10 straight
subplot(2,2,3);
plot(datas_cl(1:19,1), [results_2(1:19,[1,3,4])-datas_cl(1:19,4)])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Error Cl vs Angle of Attack for beta =  10 straight');
xlabel('AoA [deg]');
ylabel('Cl ');
legend("thin airfoil method 1","thin airfoil method 2", "joukowski method");
%-----------------------beta 10 curved
subplot(2,2,4);
plot(datas_cl(1:19,1), [results_2(1:19,[2,5])-datas_cl(1:19,5)])
hAxis = gca;
hAxis.XAxisLocation = 'origin';
hAxis.YAxisLocation = 'origin';
grid on
title('Error Cl vs Angle of Attack for beta =  10 curved');
xlabel('AoA [deg]');
ylabel('Cl ');
legend("thin airfoil method 1","joukowski method");

%% ERRORS QUALIFICATION
%close all

%-----------------------------cl vs AoA
fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'slope and origin comparison for each theories and profiles');
sgtitle('Slope and origin comparison for each curve (stall removed for measurement)')
%-----------------beta5straight
subplot(2,2,1);
xLabels = ["measured gradient (without stall)","thin foil theory1","thin foil theory2","Joukowski"];
bar(xLabels, [flip(measured(1,:))', flip(squeeze(theory(:,1,:))') ]);
title('slope and origin values comparison for beta=5 straight');
xlabel('Categories');
ylabel('Values');
legend('origin','slope');
grid on;
%-----------------beta5curved
subplot(2,2,2);
xLabels = ["measured gradient (without stall)","thin foil theory1","thin foil theory2","Joukowski"];
bar(xLabels, [flip(measured(2,:))', flip(squeeze(theory(:,2,:))') ]);
title('slope and origin values comparison for beta=5 curved');
xlabel('Categories');
ylabel('Values');
legend('origin','slope');
grid on;
%-----------------beta10straight
subplot(2,2,3);
xLabels = ["measured gradient (without stall)","thin foil theory1","thin foil theory2","Joukowski"];
bar(xLabels, [flip(measured(3,:))', flip(squeeze(theory(:,3,:))') ]);
title('slope and origin values comparison for beta=10 straight');
xlabel('Categories');
ylabel('Values');
legend('origin','slope');
grid on;
%-----------------beta10curved
subplot(2,2,4);
xLabels = ["measured gradient (without stall)","thin foil theory1","thin foil theory2","Joukowski"];
bar(xLabels, [flip(measured(4,:))', flip(squeeze(theory(:,4,:))') ]);
title('slope and origin values comparison for beta=10 curved');
xlabel('Categories');
ylabel('Values');
legend('origin','slope');
grid on;


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'relative error to the measurement for each theory');
sgtitle('relative error on slope and origin to the measurement for each theory')
%----------------------------------
subplot(1,2,1);
xLabels = ["thin foil theory1","thin foil theory2","Joukowski"];
bar(xLabels, squeeze(relative_error(:,:,1))');
title('Slope');
xlabel('Categories');
ylabel('relative error ');
legend('beta = 5 straight','beta = 5 curved','beta = 10 straight','beta = 10 curved');
grid on;
%----------------------------------
subplot(1,2,2);
xLabels = ["thin foil theory1","thin foil theory2","Joukowski"];
bar(xLabels, squeeze(relative_error(:,:,2))');
title('Origin');
xlabel('Categories');
ylabel('relative error ');
legend('beta = 5 straight','beta = 5 curved','beta = 10 straight','beta = 10 curved');
grid on;

% difference curved/straight
diff_curve_5_cl = mean(datas_cl(1:19,3)-datas_cl(1:19,2));
diff_curve_10_cl = mean(datas_cl(1:19,5)-datas_cl(1:19,4));
figure;
xLabels = ["Beta = 5","beta = 10"];
bar(xLabels, [[diff_curve_5_cl;theory(1,2,2)-theory(1,1,2);theory(3,2,2)-theory(3,1,2)], [diff_curve_10_cl;theory(1,4,2)-theory(1,3,2);theory(3,4,2)-theory(3,3,2)]]);
title('influence of curvature : deltaCl vs theory & measurement');
xlabel('Categories');
ylabel('delta Cl ');
legend('paper','thin foil 1','Joukowski');
grid on;

% difference straight/curved
figure;
xLabels = ["AoA = 0°","AoA = 2°","AoA = 4°","AoA = 6°"];
bar(xLabels, [[-mean(datas_cp(:,3)-datas_cp(:,5)-(datas_cp(:,4)-datas_cp(:,6)));mean(results_3(2:sz_theta,3)-results_3(2:sz_theta,2))], ...
    [-mean(datas_cp(:,7)-datas_cp(:,9)-(datas_cp(:,8)-datas_cp(:,10)));         mean(results_3(2:sz_theta,5)-results_3(2:sz_theta,4))], ...
    [-mean(datas_cp(:,11)-datas_cp(:,13)-(datas_cp(:,12)-datas_cp(:,14)));      mean(results_3(2:sz_theta,7)-results_3(2:sz_theta,6))], ...
    [-mean(datas_cp(:,15)-datas_cp(:,17)-(datas_cp(:,16)-datas_cp(:,18)));      mean(results_3(2:sz_theta,9)-results_3(2:sz_theta,8))]]); 
title('influence of curvature : deltaCp vs theory & measurement');
xlabel('Categories');
ylabel('delta Cp ');
legend('paper','thin foil 1');
grid on;

%% CREATIVE PART
%close all

error11=[];
error12=[];
error13=[];
error14=[];
error21=[];
error22=[];
error23=[];
error24=[];
errorcp1=[];
errorcp2=[];
errorcp3=[];

for p=1:10%0.1:0.1:1
    profile=deform(X,"straight",5,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(1,:), sum_cp_s,p/10,AoA,X,5);
    error11(:,p) = error(1:3,1);
    error21(:,p) = error(1:3,2);

    profile=deform(X,"curved",5,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(2,:), sum_cp_c,p/10,AoA,X,5);
    error12(:,p) = error(1:3,1);
    error22(:,p) = error(1:3,2);

    profile=deform(X,"straight",10,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(3,:), sum_cp_s,p/10,AoA,X,10);
    error13(:,p) = error(1:3,1);
    error23(:,p) = error(1:3,2);
    errorcp1(1:4,p) = error(4:7,1);

    profile=deform(X,"curved",10,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(4,:), sum_cp_c,p/10,AoA,X,10);
    error14(:,p) = error(1:3,1);
    error24(:,p) = error(1:3,2);
    errorcp1(5:8,p) = error(4:7,1);
end


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation p slope');
sgtitle('relative errors on Cl slope varying the rotation point')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:1,error11)
scatter(0.7,relative_error(:,1,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:1,error12)
scatter(0.7,relative_error(:,2,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:1,error13)
scatter(0.7,relative_error(:,3,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:1,error14)
scatter(0.7,relative_error(:,4,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation p origin');
sgtitle('relative errors on Cl origin varying the rotation point')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl origin ');
xlabel('rotation point ');
plot(0.1:0.1:1,error21)
scatter(0.7,relative_error(:,1,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl origin ');
xlabel('rotation point ');
plot(0.1:0.1:1,error22)
scatter(0.7,relative_error(:,2,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl origin ');
xlabel('rotation point ');
plot(0.1:0.1:1,error23)
scatter(0.7,relative_error(:,3,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl origin');
xlabel('rotation point ');
plot(0.1:0.1:1,error24)
scatter(0.7,relative_error(:,4,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


for b=0:20
    profile=deform(X,"straight",b,profile_0,0.7,2.5);
    [results,error]=test_profile(profile,measured(1,:), sum_cp_s,0.7,AoA,X,b);
    error11(:,b+1) = error(1:3,1);
    error21(:,b+1) = error(1:3,2);

    profile=deform(X,"curved",b,profile_0,0.7,2.5);
    [results,error]=test_profile(profile,measured(2,:), sum_cp_c,0.7,AoA,X,b);
    error12(:,b+1) = error(1:3,1);
    error22(:,b+1) = error(1:3,2);

    profile=deform(X,"straight",b,profile_0,0.7,2.5);
    [results,error]=test_profile(profile,measured(3,:), sum_cp_s,0.7,AoA,X,b);
    error13(:,b+1) = error(1:3,1);
    error23(:,b+1) = error(1:3,2);
    errorcp2(1:4,b+1) = error(4:7,1);

    profile=deform(X,"curved",b,profile_0,0.7,2.5);
    [results,error]=test_profile(profile,measured(4,:), sum_cp_c,0.7,AoA,X,b);
    error14(:,b+1) = error(1:3,1);
    error24(:,b+1) = error(1:3,2);
    errorcp2(5:8,b+1) = error(4:7,1);
end


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation beta slope');
sgtitle('relative errors on Cl slope varying beta')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl slope ');
xlabel('beta [deg]');
plot(0:20,error11)
scatter(5,relative_error(:,1,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl slope ');
xlabel('beta [deg]');
plot(0:20,error12)
scatter(5,relative_error(:,2,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl slope ');
xlabel('beta [deg]');
plot(0:20,error13)
scatter(10,relative_error(:,3,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl slope ');
xlabel('beta [deg]');
plot(0:20,error14)
scatter(10,relative_error(:,4,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation beta origin');
sgtitle('relative errors on Cl origin varying beta')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl origin ');
xlabel('beta [deg]');
plot(0:20,error21)
scatter(5,relative_error(:,1,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl origin ');
xlabel('beta [deg]');
plot(0:20,error22)
scatter(5,relative_error(:,2,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl origin ');
xlabel('beta [deg]');
plot(0:20,error23)
scatter(10,relative_error(:,3,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl origin ');
xlabel('beta [deg]');
plot(0:20,error24)
scatter(10,relative_error(:,4,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


for n=1:21%1:5
    profile=deform(X,"straight",5,profile_0,0.7,0.2*n+0.8);
    [results,error]=test_profile(profile,measured(1,:), sum_cp_s,0.7,AoA,X,5);
    error11(:,n) = error(1:3,1);
    error21(:,n) = error(1:3,2);

    profile=deform(X,"curved",5,profile_0,0.7,0.2*n+0.8);
    [results,error]=test_profile(profile,measured(2,:), sum_cp_c,0.7,AoA,X,5);
    error12(:,n) = error(1:3,1);
    error22(:,n) = error(1:3,2);

    %INUTILE
    profile=deform(X,"straight",10,profile_0,0.7,0.2*n+0.8);
    [results,error]=test_profile(profile,measured(3,:), sum_cp_s,0.7,AoA,X,10);
    error13(:,n) = error(1:3,1);
    error23(:,n) = error(1:3,2);
    errorcp3(1:4,n) = error(4:7,1);

    profile=deform(X,"curved",10,profile_0,0.7,0.2*n+0.8);
    [results,error]=test_profile(profile,measured(4,:), sum_cp_c,0.7,AoA,X,10);
    error14(:,n) = error(1:3,1);
    error24(:,n) = error(1:3,2);
    errorcp3(5:8,n) = error(4:7,1);
end

fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation curve slope');
sgtitle('relative errors on Cl slope varying curve n')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl slope ');
xlabel('curve n ');
plot(1:0.2:5,error11)
scatter(2.5,relative_error(:,1,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl slope ');
xlabel('curve n ');
plot(1:0.2:5,error12)
scatter(2.5,relative_error(:,2,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl slope ');
xlabel('curve n ');
plot(1:0.2:5,error13)
scatter(2.5,relative_error(:,3,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl slope ');
xlabel('curve n ');
plot(1:0.2:5,error14)
scatter(2.5,relative_error(:,4,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation curve origin');
sgtitle('relative errors on Cl origin varying curve n')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl origin ');
xlabel('curve n ');
plot(1:0.2:5,error21)
scatter(1,relative_error(:,1,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl origin ');
xlabel('curve n ');
plot(1:0.2:5,error22)
scatter(2.5,relative_error(:,2,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl origin ');
xlabel('curve n ');
plot(1:0.2:5,error23)
scatter(1,relative_error(:,3,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl origin ');
xlabel('curve n ');
plot(1:0.2:5,error24)
scatter(2.5,relative_error(:,4,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on

error11=[];
error12=[];
error13=[];
error14=[];
error21=[];
error22=[];
error23=[];
error24=[];

a5=tan(deg2rad(5))*(1-pivot_point);
a10=tan(deg2rad(10))*(1-pivot_point);

for p=1:9%0.1:0.1:1
    beta5 = rad2deg(atan2(a5,1-p/10));
    beta10 = rad2deg(atan2(a10,1-p/10));
    profile=deform(X,"straight",beta5,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(1,:), sum_cp_s,p/10,AoA,X,beta5);
    error11(:,p) = error(1:3,1);
    error21(:,p) = error(1:3,2);

    profile=deform(X,"curved",beta5,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(2,:), sum_cp_c,p/10,AoA,X,beta5);
    error12(:,p) = error(1:3,1);
    error22(:,p) = error(1:3,2);

    profile=deform(X,"straight",beta10,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(3,:), sum_cp_s,p/10,AoA,X,beta10);
    error13(:,p) = error(1:3,1);
    error23(:,p) = error(1:3,2);
    errorcp4(1:4,p) = error(4:7,1);

    profile=deform(X,"curved",beta10,profile_0,p/10,2.5);
    [results,error]=test_profile(profile,measured(4,:), sum_cp_c,p/10,AoA,X,beta10);
    error14(:,p) = error(1:3,1);
    error24(:,p) = error(1:3,2);
    errorcp4(5:8,p) = error(4:7,1);
end


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation p slope');
sgtitle('relative errors on Cl slope varying the rotation point and beta (final point constant)')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error11)
scatter(0.7,relative_error(:,1,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error12)
scatter(0.7,relative_error(:,2,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error13)
scatter(0.7,relative_error(:,3,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl slope ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error14)
scatter(0.7,relative_error(:,4,1),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation p origin');
sgtitle('relative errors on Cl origin varying the rotation point and beta (final point constant)')
%-----------------------
subplot(2,2,1);
hold on
title('relative error to straight profile beta 5');
ylabel('relative error Cl origin ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error21)
scatter(0.7,relative_error(:,1,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('relative error to curved profile beta 5');
ylabel('relative error Cl origin ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error22)
scatter(0.7,relative_error(:,2,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,3);
hold on
title('relative error to straight profile beta 10');
ylabel('relative error Cl origin ');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error23)
scatter(0.7,relative_error(:,3,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on
%-----------------------
subplot(2,2,4);
hold on
title('relative error to curved profile beta 10');
ylabel('relative error Cl origin');
xlabel('rotation point ');
plot(0.1:0.1:0.9,error24)
scatter(0.7,relative_error(:,4,2),"+","r")
legend("thin foil 1", "thin foil 2", "Joukowski","profile tested")
grid on


%plot errors cp
fig = figure('WindowState', 'maximized');
set(fig, 'Name', 'variation to error on cp');
sgtitle('relative errors on Cp')
%-----------------------
subplot(2,2,1);
hold on
title('varying rotation point p');
ylabel('relative error on deltaCp ');
xlabel('rotation point');
plot(0.1:0.1:1,errorcp1(5:8,:))
legend("error with AoA = 0", "error with AoA = 2","error with AoA = 4","error with AoA = 6")
grid on
%-----------------------
subplot(2,2,2);
hold on
title('varying beta');
ylabel('relative error on deltaCp ');
xlabel('beta');
plot(0:20,errorcp2(5:8,:))
legend("error with AoA = 0", "error with AoA = 2","error with AoA = 4","error with AoA = 6")
grid on
%-----------------------
subplot(2,2,3:4);
hold on
title('varying curve n');
ylabel('relative error on deltaCp ');
xlabel('curve power');
plot(1:0.2:5,errorcp3(5:8,:))
legend("error with AoA = 0", "error with AoA = 2","error with AoA = 4","error with AoA = 6")
grid on