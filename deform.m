function profile_x = deform(X,type, beta, profile_0, x0,n)

profile_x=[profile_0];

i=1;
while(x0<X(i))
    if type == "straight"
        profile_x(i,1) = (X(i)-x0)*sin(-deg2rad(beta));
    elseif type == "curved"
        profile_x(i,1) = (X(i)-x0)^n*sin(-deg2rad(beta))/((X(1)-x0)^(n-1));
    end
    i = i+1;
end

profile_x(:,2:3) = profile_x(:,2:3) + profile_x(:,1);

%rotating foil
angle = atan(-profile_x(1,1));
Rotm = [[cos(angle), -sin(angle)];[sin(angle), cos(angle)]];

for i=1:height(X)
    profile_x(i,4) = Rotm(1,:)*[X(i);profile_x(i,1)];
    profile_x(i,5) = Rotm(2,:)*[X(i);profile_x(i,1)];
    profile_x(i,6) = Rotm(1,:)*[X(i);profile_x(i,2)];
    profile_x(i,7) = Rotm(2,:)*[X(i);profile_x(i,2)];
    profile_x(i,8) = Rotm(1,:)*[X(i);profile_x(i,3)];
    profile_x(i,9) = Rotm(2,:)*[X(i);profile_x(i,3)];
end

% remettre à 1 de cord

end