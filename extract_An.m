function A = extract_An(profile, n)

c = coeffvalues(fit(profile(:,1), profile(:,2), 'poly'+string(n)));%ou est l origine ? -0.5
c = c.*[n:-1:0];%[3, 2, 1, 0];

%projection+derivee du polynome
fun0 =  @(theta) ((1-cos(theta))/2).^[n-1:-1:-1]*c'./pi;

A = integral(fun0,0,pi, 'ArrayValued',true);%
for i=1:n
    funn = @(theta) 2*cos(i*theta)/pi.*(((1-cos(theta))/2).^[n-1:-1:-1]*c');
    A(i+1) = integral(funn,0,pi, 'ArrayValued',true);%
end

end