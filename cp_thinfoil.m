function [C_p, k] = cp_thinfoil(profile, n, u_inf, theta, alpha)

% fit & integer camberline
A = extract_An(profile,n);

%thin airfoil pressure coeff
k = 2*u_inf*(alpha-A(1))*((1+cos(theta))./sin(theta));
for i = 1:n
    k= k + 2*u_inf*A(i+1).*sin(i*theta);
end
C_p = 2*k/(u_inf);

end